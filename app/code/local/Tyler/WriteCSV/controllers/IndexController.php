<?php
class Tyler_WriteCSV_IndexController extends Mage_Core_Controller_Front_Action{
    public function indexAction()
{
//Get current layout state
$this->loadLayout();
$block = $this->getLayout()->createBlock(
'Mage_Core_Block_Template',
'Content',
array('template' => 'tyler/writeContent.phtml')
);
$this->getLayout()->getBlock('content')->append($block);
//Release Layout Stream
$this->renderLayout();
}
}
?>